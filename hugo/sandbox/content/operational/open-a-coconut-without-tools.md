+++
categories = ["Operational"]
date = "2016-10-01T17:41:00-06:00"
description = ""
roles = ["Store Manager"]
tags = []
title = "Open a coconut without tools"
type = "procedure"

+++
1. Open the coconut

![](/uploads/2017/05/08/How-To-Open-A-Coconut-With-No-Tools-In-A-Survival-Situation.jpg)

1. Enjoy

<iframe width="100%" height="auto" src="https://www.youtube.com/embed/T9bXoOa1qTg" frameborder="0" allowfullscreen="" async="" preload=""></iframe>


Dance !!!!!!