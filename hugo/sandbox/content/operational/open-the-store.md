+++
categories = ["Operational"]
date = "2016-12-01T17:34:00-07:00"
description = ""
roles = ["Store Manager"]
tags = []
title = "Open the store"
type = "procedure"

+++
1. Turn off the alarm

1. Turn on the lights

1. Prepare the cash registers

1. Unlock the doors to let clients in

