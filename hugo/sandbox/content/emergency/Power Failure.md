+++
categories = ["Emergency"]
date = "2017-05-06T18:06:05-06:00"
description = ""
roles = ["Store Operator", "Store Manager"]
tags = []
title = "Power Failure"
type = "procedure"

+++
1. Check for clients trapped in elevators if applicable.  Contact your local fire department or elevator emergency line to release passengers.

1. Ensure there are no injuries.

1. Contact your local utility company or mall office to be notified when they anticipate service to be restored.

1. Shut down any equipment that could be damaged when power is restored by flipping the breaker to the `Off` position

1. etc...