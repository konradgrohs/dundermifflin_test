+++
categories = ["Emergency"]
date = "2017-05-07T21:10:57Z"
description = ""
roles = ["Store Manager"]
tags = []
title = "Burglary"
type = "procedure"

+++
### If you arrive at the store and discover there's been a burglary

1. Call 911
2. Do not enter the store until the police arrive and search the store
3. After the police say it's safe to enter the store, determine if anything is missing
4. Make a list of the stolen items
5. Check for damage to the store
   * Broken windows
   * Borken locks
   * Damage to the store
   * Damage to fixures
   * Damage to merchandise
6. Contact the Helpdesk (1-855-353-8555) to inform them of the situation
7. Next
8. Next step
9. Step 9
10. Step 10
11. Step 11
12. Step 12
13. Step 13
14. Step 14