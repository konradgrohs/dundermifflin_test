+++
categories = ["Emergency"]
date = "2017-05-06T19:29:25-06:00"
description = ""
roles = ["Store Manager", "Cast Member"]
tags = []
title = "Robbery"
type = "procedure"

+++
### *If a robber is trying to rob the store / cash register*
1. Comply with the demands of the robber
2. Do exactly as you are told and to not hessitate
3. Inform the robber ahead of time when making any movements   
* `"I am reaching into a bag."  "I am reaching into a drawer"`
4. Inform the robber if there is someone in the backstage area that might enter

