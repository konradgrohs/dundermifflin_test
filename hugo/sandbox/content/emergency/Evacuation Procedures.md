+++
categories = ["Emergency"]
date = "2017-05-06T15:14:35-06:00"
description = ""
roles = ["Store Operator"]
tags = []
title = "Evacuation Procedures"
type = "procedure"

+++
1. Assign a cast member to each fire exit  
2. Order an evacuation of the store.  
   Here is a sample announcement: |
   --- |  
   `"May I please have your attention?  An emergency in the store makes it necessary to evactuate the store immediately. Please move calmly to the nearest emergency exit.  Thank you."` |   
3. Meet at the pre-determined primary or secondary meeting points.  
4. Ensure all areas of the store have been evacuated.   
5. Verify that all cast members are outside the store.   
6. Assign a cast member to meet local authorities and get direction from them.   
7. Follow the direction of local authorities.  
8. Contact the Helpdesk (1-855-353-8555) to inform them of the situation so an FSC Disaster Recovery Team member can be paged to assist you if necessary.   
9. Contact DM/RD and inform them of the situation.
10. Contact the Sephora Maintenance Hotline (1-877-684-3657) if there is major damage to the store.



